<?php
    require_once('include/db.php');
    require_once('include/session.php');
    require_once('include/functions.php');
?>

<?php

    if(isset($_POST['submit'])){
        $name=mysql_real_escape_string($_POST["name"]);
        $email=mysql_real_escape_string($_POST["email"]);
        $comment=mysql_real_escape_string($_POST["comment"]);


        date_default_timezone_set("Africa/Lagos");
        $currenttime=time();
        $datetime=strftime("%B-%d-%Y-%H-%M-%S",$currenttime);
        $datetime;

        $postid=$_GET['id'];

        if(empty($name) || empty(email) || empty(comment)){
            $_SESSION["ErrorMessage"]= "Name Can't Be Empty";

        }elseif(strlen($comment)>500){
            $_SESSION["ErrorMessage"]="Too Long Comment";

        } else{
            global $connectingDB;
            $Query="INSERT INTO comment (datetime, name, email, comment, status)
            VALUES('$datetime','$name','$email','$comment','OFF')";
            $Execute=mysql_query($Query);


            if($Execute){
                $_SESSION["SuccessMessage"]="Comment Submited Successfully";
                Redirect_to("fullpost.php?id= {$postid}");
            } else{
                $_SESSION["ErrorMessage"]="Comment Failed To Add";
                Redirect_to("dashboard.php");
            }
        }
    }


?>

<!DOCTYPE html>
<html lang="en">

<head>
    <title>Full Blog Post</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="css/blogstyle.css">

    <style>
        .col-sm-3{
            background-color: red;
        }

        .blogpost{
          background-color: #f5f5f5;
          padding-left: 10px;
          padding-right: 10px;
          padding-top: 10px;
          overflow: hidden;
        }
    </style>

</head>

<body>
            <!-- blue line above the navbar -->
    <div style="height: 10px; background-color: #27aae1;"></div>

            <!-- navigation -->
    <nav class="navbar navbar-inverse" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a href="blog.php" class="navbar-brand">E#3</a>
            </div>
            <div class="collapse navbar-collapse" id="collapse">
                <ul class="nav navbar-nav">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">About</a></li>
                    <li><a href="#">Blog</a></li>
                    <li><a href="#">Feature</a></li>
                    <li><a href="#">Contact</a></li>
                </ul>
                <form action="blog.php" class="navbar-form navbar-right">
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Search" name="search">
                    </div>
                    <button class="btn btn-default" name="searchbutton">Go</button>
                </form>
            </div><!-- navbar-collapse ending -->

        </div><!-- container -->
    </nav><!-- nav ending -->
            <!-- blue line below the navbar -->
    <div class="line" style="height: 10px; background-color: #27aae1;"></div>

              <!-- Main content -->
    <div class="container">
        <h1>The Complete Responsive CMS</h1>
        <p class="lead">The complete blog using php</p>

    <div class="row">
        <div class="col-sm-8">

            <?php
                global $connectingDB;
                if(isset($_GET["searchbutton"])){
                  $search = $_GET["search"];
                  $ViewQuery=" SELECT * FROM admin_panel
                  WHERE datetime LIKE '%$search%' OR title LIKE '%$search%' OR category LIKE '%$search%'
                  OR post LIKE '%$search%' ";
                } else{
                    $PostIdFromUrl = $_GET["id"];
                $ViewQuery="  SELECT * FROM admin_panel WHERE id='$PostIdFromUrl' ORDER BY datetime desc";}
                $Execute=mysql_query($ViewQuery);
                while ($Datarows = mysql_fetch_array($Execute)) {
                    $id=$Datarows['id'];
                    $datetime=$Datarows['datetime'];
                    $title=$Datarows['title'];
                    $category=$Datarows['category'];
                    $admin=$Datarows['author'];
                    $image=$Datarows['image'];
                    $post=$Datarows['post'];

             ?>

                <div class="blogpost thumbnail">
                    <img class="img-responsive img-rounded"src="upload/<?php echo $image; ?>">
                    <div class="caption">
                        <h2 id="heading"><?php echo htmlentities($title); ?></h2>
                        <p class="description">Category:<?php echo htmlentities($category); ?> | Publish on
                            <?php echo $datetime; ?>
                        </P>
                        <p> class="post">
                            <?php echo ($post); ?>

                        </p>

                    </div>

                </div>
            <?php } ?>


            <br><br
            <br><br>
            <strong>Share you thoghtr</strong>
            <br><br>

            <form action="fullpost.php?id=<?php echo $_GET['$postid']; ?>" method="post" enctype="multipart/form-data">
                <fieldset>
                    <div class="form-group">
                        <label for="name">Name:</label>
                        <input type="text" class="form-control" name="name" id="name" placeholder="name">
                    </div>

                    <div class="form-group">
                        <label for="email">Email:</label>
                        <input type="text" class="form-control" name="email" id="email" placeholder="Email">
                    </div>

                    <div class="form-group">
                        <label for="comment"><span class="fiedinfo">Comment</span></label>
                        <textarea class="form-control" name="comment" id="comment"></textarea>
                    </div>
                    <br>
                    <input type="submit" class="btn btn-primary" name="submit" value="Submit">
                    <br><br>
                    <br><br>
                </fieldset>

            </form>

        </div><!-- col-sm-8 endiing Main Area-->

        <div class="col-sm-offset-1 col-sm-3">
            <h2>Test</h2>
            <p>Programs rarely work correctly the first time. Many things can go wrong in your program that cause the PHP interpreter to generate an error message.
              You have a choice about where those error messages go. The messages can be sent along with other program output to the web browser.
              They can also be included in the web server error log.
              To make error messages display in the browser, set the display_errors configuration directive to On. To send errors to the web server error log, set log_errors to On.
              You can set them both to On if you want error messages in both places.
            </p>
        </div><!-- col-sm-3 endig Side Area-->

    </div><!-- row ending -->
</div><!-- container -->

<div id="footer">
    <hr>
    <p>Theme By | Em3 | &copy;2017</p>
    <a style="color: white; text-decoration: none; cusor: pointer; font-weight: bold;"


</div>

</body>
</html>
