<?php
    require_once('include/db.php');
    require_once('include/session.php');
    require_once('include/db.php');
?>

<!DOCTYPE html>
<html lang="en">

<head>
        <!-- meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- title tag -->
    <title> Admin Dashboard</title>

    <!-- font awesome -->
   <!-- <link rel="stylesheet" href="css/font-awesome/css/font-awesome.min.css">-->

    <!-- bootstrap css -->
    <link rel="stylesheet" href="css/bootstrap.min.css">

    <!-- style css -->
    <link rel="stylesheet" href="css/style.css">

</head>


<body>

  <!-- blue line above the navbar -->
<div style="height: 10px; background-color: #27aae1;"></div>

    <!-- navigation -->
    <nav class="navbar navbar-inverse" role="navigation">
        <div class="container">
          <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#collapse">
                  <span class="sr-only">Toggle Navigation</span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
              </button>
              <a href="blog.php" class="navbar-brand">E#3</a>
          </div>
          <div class="collapse navbar-collapse" id="collapse">
              <ul class="nav navbar-nav">
                  <li><a href="#">Home</a></li>
                  <li><a href="#">About</a></li>
                  <li><a href="#">Blog</a></li>
                  <li><a href="#">Feature</a></li>
                  <li><a href="#">Contact</a></li>
              </ul>
              <form action="blog.php" class="navbar-form navbar-right">
                  <div class="form-group">
                      <input type="text" class="form-control" placeholder="Search" name="search">
                  </div>
                  <button class="btn btn-default" name="searchbutton">Go</button>
              </form>
          </div><!-- navbar-collapse ending -->

        </div><!-- container -->
    </nav><!-- nav ending -->
    <!-- blue line below the navbar -->
  <div class="line" style="height: 10px; background-color: #27aae1;"></div>


    <div class="container-fluid">
        <div class="row">

            <div class="col-sm-2">
              <br>

                <ul id="side-menu" class="nav nav-pills nav-sticked">
                    <li class="active">
                        <a href="dashboard.php"><span class="glyphicon glyphicon-th"> </span> Dashboard </a>
                    </li>

                    <li>
                        <a href="add-new-post.php"><span class="glyphicon glyphicon-list-alt"></span>&nbsp; Add New Post</a>
                    </li>

                    <li>
                        <a href="category.php"><span class="glyphicon glyphicon-tag"></span>&nbsp; Category</a>
                    </li>

                    <li>
                        <a href=""><span class="glyphicon glyphicon-user"></span>&nbsp; Manage Admin</a>
                    </li>

                    <li>
                        <a href=""><span class="glyphicon glyphicon-comment"></span>&nbsp; Comments</a>
                    </li>

                    <li>
                        <a href=""><span class="glyphicon glyphicon-list"></span>&nbsp; Live blog</a>
                    </li>

                    <li>
                        <a href=""><span class="glyphicon glyphicon-log-out"></span>&nbsp; Logout</a>
                    </li>

                </ul>

            </div><!-- ending of side area -->

            <div class="col-sm-10">
                <h2>immanuel</h2>
                 <div><?php echo Message();
                            echo SuccessMessage();
                     ?>
                </div>

                <div class="table-responsive">

                    <table class="table table-striped table-hover">
                        <tr>
                            <th>No</th>
                            <th>Post Title</th>
                            <th>Date & Time</th>
                            <th>Author</th>
                            <th>Category</th>
                            <th>Banner</th>
                            <th>Comments</th>
                            <th>Action</th>
                            <th>details</th>
                        </tr>

                        <?php
                            $idno=0;
                            $connectingDB;
                            $ViewQuery="  SELECT * FROM admin_panel ORDER BY datetime desc";
                            $Execute=mysql_query($ViewQuery);
                            while ($Datarows = mysql_fetch_array($Execute)) {
                                $id=$Datarows['id'];
                                $datetime=$Datarows['datetime'];
                                $title=$Datarows['title'];
                                $category=$Datarows['category'];
                                $admin=$Datarows['author'];
                                $image=$Datarows['image'];
                                $post=$Datarows['post'];
                                $idno++;
                         ?>

                      <tr>
                          <td><?php echo $idno; ?></td>
                          <td style="color:blue;"><?php
                              if(strlen($title)>15){$title=substr($title,0,15);}
                              echo $title; ?></td>
                          <td><?php
                              if(strlen($datetime)>11){$datetime=substr($datetime,0,11);}
                              echo $datetime;
                            ?>
                          </td>
                          <td><?php echo $admin; ?></td>
                          <td><?php echo $category; ?></td>
                          <td><img src="upload/<?php echo $image; ?>" width="100" height="60"></td>
                          <td>processing</td>
                          <td>
                            <a href="edit-post.php?edit=<?php echo $id ?>" target="_blank"
                              <span class="btn btn-info">Edit</span>
                            </a>
                            <a href="delete-post.php?delete=<?php echo $id ?>" target="_blank"
                              <span class="btn btn-danger">Delete</span>
                            </a>
                          </td>
                          <td>
                            <a href="fullpost.php?id=<?php echo $id ?>" target="_blank"
                              <span class="btn btn-info">Live preview</span>
                            </a>
                          </td>
                      </tr>


                    <?php } ?>
                    </table>

                </div>

            </div><!-- ending of main area -->

        </div><!-- ending of row -->
    </div><!-- ending of container -->

    <div id="footer">
        <hr>
        <p>Theme By | Em3 | &copy;2017</p>
        <a style="color: white; text-decoration: none; cusor: pointer; font-weight: bold;"


    </div>











    <!-- jquery -->
   <!-- <script src="js/jquery.js"></script>-->

    <!-- bootstrap js -->
    <script src="js/bootstrap.min.js"></script>

</body>


</html>
