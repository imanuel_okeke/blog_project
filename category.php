<?php
    require_once('include/db.php');
    require_once('include/session.php');
    require_once('include/functions.php');
?>

<?php
    $admin="immanuel";

    if(isset($_POST['submit'])){
        $category=mysql_real_escape_string($_POST["category"]);

        date_default_timezone_set("Africa/Lagos");
        $currenttime=time();
        $datetime=strftime("%B-%d-%Y-%H-%M-%S",$currenttime);
        $datetime;

        if(empty($category)){
            $_SESSION["ErrorMessage"]= "All Fields Must Be Filled";
            Redirect_to("category.php");

        } elseif(strlen($category)>50){
            $_SESSION["ErrorMessage"]="Too long Name";
            Redirect_to("category.php");

        } else{
            global $connectingDB;
            $Query="INSERT INTO category (datetime, name, creatorname)
            VALUES('$datetime','$category','$admin')";
            $Execute=mysql_query($Query);

            if($Execute){
                $_SESSION["SuccessMessage"]="Category Added Successfully";
                Redirect_to("category.php");
            } else{
                $_SESSION["ErrorMessage"]="Category Failed To Add";
                Redirect_to("category.php");
            }
        }
    }


?>

<!DOCTYPE html>
<html lang="en">

<head>
        <!-- meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- title tag -->
    <title> Admin Dashboard</title>

    <!-- font awesome -->
   <!-- <link rel="stylesheet" href="css/font-awesome/css/font-awesome.min.css">-->

    <!-- bootstrap css -->
    <link rel="stylesheet" href="css/bootstrap.min.css">

    <!-- style css -->
    <link rel="stylesheet" href="css/style.css">

</head>


<body>

    <div class="container-fluid">
        <div class="row">

            <div class="col-sm-2">
                <h2>emmanuel</h2>

                <ul id="side-menu" class="nav nav-pills nav-sticked">
                    <li>
                        <a href="dashboard.php"><span class="glyphicon glyphicon-th"> </span> Dashboard </a>
                    </li>

                    <li>
                        <a href="add-new-post.php"><span class="glyphicon glyphicon-list-alt"></span>&nbsp; Add New Post</a>
                    </li>

                    <li class="active">
                        <a href="category.php"><span class="glyphicon glyphicon-tag"></span>&nbsp; Category</a>
                    </li>

                    <li>
                        <a href=""><span class="glyphicon glyphicon-user"></span>&nbsp; Manage Admin</a>
                    </li>

                    <li>
                        <a href=""><span class="glyphicon glyphicon-comment"></span>&nbsp; Comments</a>
                    </li>

                    <li>
                        <a href=""><span class="glyphicon glyphicon-list"></span>&nbsp; Live blog</a>
                    </li>

                    <li>
                        <a href=""><span class="glyphicon glyphicon-log-out"></span>&nbsp; Logout</a>
                    </li>

                </ul>

            </div><!-- ending of side area -->

            <div class="col-sm-10">
                <h2>Manage Categories</h2>

                <div><?php echo Message();
                            echo SuccessMessage();
                     ?>
                </div>

                <form action="category.php" method="post">
                    <fieldset>
                        <div class="form-group">
                            <label for="categoryname">Name:</label>
                            <input type="text" class="form-control" name="category" id="categoryname" placeholder="Name">
                        </div>
                        <br>
                        <input type="submit" class="btn btn-success btn-block" name="submit" value="Add New Category">
                        <br>
                    </fieldset>


                </form>

                <div>

                    <table class="bordered-table table table-bordered">
                        <tr>
                          <th style="color: blue;">#id</th>
                          <th>Category</th>
                          <th>Creator Name</th>
                          <th>Date</th>
                        </tr>

                        <?php
                                //SQL TO EXTRACT db_TABLE PARAMETERS
                                $Query = "SELECT * FROM category ORDER BY datetime desc";
                                $Execute=mysql_query($Query);
                                $SrNo=0;

                                while($Datarows = mysql_fetch_array($Execute)){
                                    $id=$Datarows['id'];
                                    $category=$Datarows['name'];
                                    $creatorname=$Datarows['creatorname'];
                                    $datetime=$Datarows['datetime'];
                                    $SrNo++;
                          ?>

                        <tr>
                          <td><?php echo $SrNo; ?></td>
                          <td><?php echo $category; ?></td>
                          <td><?php echo $creatorname; ?></td>
                          <td><strong><?php echo $datetime; ?></strong> </td>
                      </tr>
                        <?php } ?>
                      </table>


                </div>

            </div><!-- ending of main area -->

        </div><!-- ending of row -->
    </div><!-- ending of container -->

    <div id="footer">
        <hr>
        <p>Theme By | Em3 | &copy;2017</p>
        <a style="color: white; text-decoration: none; cusor: pointer; font-weight: bold;"


    </div>











    <!-- jquery -->
   <!-- <script src="js/jquery.js"></script>-->

    <!-- bootstrap js -->
    <script src="js/bootstrap.min.js"></script>

</body>


</html>
