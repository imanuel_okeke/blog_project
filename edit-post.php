<?php
    require_once('include/db.php');
    require_once('include/session.php');
    require_once('include/functions.php');
?>

<?php
    $author="Emmanuel";
    $image=$_FILES["image"]["name"];
    $target="upload/".basename($_FILES["image"]["name"]);

    if(isset($_POST['submit'])){
        $title=mysql_real_escape_string($_POST["title"]);
        $category=mysql_real_escape_string($_POST["category"]);
        $post=mysql_real_escape_string($_POST["post"]);


        date_default_timezone_set("Africa/Lagos");
        $currenttime=time();
        $datetime=strftime("%B-%d-%Y-%H-%M-%S",$currenttime);
        $datetime;

        if(empty($title)){
            $_SESSION["ErrorMessage"]= "Title Can't Be Empty";
            Redirect_to("add-new-post.php");

        } elseif(strlen($title)<2){
            $_SESSION["ErrorMessage"]="Too Short Title";
            Redirect_to("add-new-post.php");

        } elseif(empty($post)){
            $_SESSION["ErrorMessage"]= "Post Can't Be Empty";
            Redirect_to("add-new-post.php");

        } else{
            global $connectingDB;
            $editFromUrl = $_GET['edit'];
            $Query="UPDATE admin_panel SET datetime='$datetime' ,title='$title'
            , category='$category' , author='$author' , image='$image' , post='$post'
             WHERE id='$editFromUrl'";
            $Execute=mysql_query($Query);

            move_uploaded_file($_FILES["image"]["tmp_name"],$target);

            if($Execute){
                $_SESSION["SuccessMessage"]="Post Updated Successfully";
                Redirect_to("dashboard.php");
            } else{
                $_SESSION["ErrorMessage"]="Post Failed To Update";
                Redirect_to("dashboard.php");
            }
        }
    }


?>

<!DOCTYPE html>
<html lang="en">

<head>
        <!-- meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- title tag -->
    <title> Edit Post</title>

    <!-- font awesome -->
   <!-- <link rel="stylesheet" href="css/font-awesome/css/font-awesome.min.css">-->

    <!-- bootstrap css -->
    <link rel="stylesheet" href="css/bootstrap.min.css">

    <!-- style css -->
    <link rel="stylesheet" href="css/style.css">

</head>


<body>

  <!-- blue line above the navbar -->
<div style="height: 10px; background-color: #27aae1;"></div>

    <!-- navigation -->
    <nav class="navbar navbar-inverse" role="navigation">
        <div class="container">
          <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#collapse">
                  <span class="sr-only">Toggle Navigation</span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
              </button>
              <a href="blog.php" class="navbar-brand">E#3</a>
          </div>
          <div class="collapse navbar-collapse" id="collapse">
              <ul class="nav navbar-nav">
                  <li><a href="#">Home</a></li>
                  <li><a href="#">About</a></li>
                  <li><a href="#">Blog</a></li>
                  <li><a href="#">Feature</a></li>
                  <li><a href="#">Contact</a></li>
              </ul>
              <form action="blog.php" class="navbar-form navbar-right">
                  <div class="form-group">
                      <input type="text" class="form-control" placeholder="Search" name="search">
                  </div>
                  <button class="btn btn-default" name="searchbutton">Go</button>
              </form>
          </div><!-- navbar-collapse ending -->

        </div><!-- container -->
    </nav><!-- nav ending -->
    <!-- blue line below the navbar -->
  <div class="line" style="height: 10px; background-color: #27aae1;"></div>


    <div class="container-fluid">
      <div class="row">

            <div class="col-sm-2">
                <h2>emmanuel</h2>

                <ul id="side-menu" class="nav nav-pills nav-sticked">
                    <li>
                        <a href="dashboard.php"><span class="glyphicon glyphicon-th"> </span> Dashboard </a>
                    </li>

                    <li class="active">
                        <a href="add-new-post.php"><span class="glyphicon glyphicon-list-alt"></span>&nbsp; Add New Post</a>
                    </li>

                    <li>
                        <a href="category.php"><span class="glyphicon glyphicon-tag"></span>&nbsp; Category</a>
                    </li>

                    <li>
                        <a href=""><span class="glyphicon glyphicon-user"></span>&nbsp; Manage Admin</a>
                    </li>

                    <li>
                        <a href=""><span class="glyphicon glyphicon-comment"></span>&nbsp; Comments</a>
                    </li>

                    <li>
                        <a href=""><span class="glyphicon glyphicon-list"></span>&nbsp; Live blog</a>
                    </li>

                    <li>
                        <a href=""><span class="glyphicon glyphicon-log-out"></span>&nbsp; Logout</a>
                    </li>

                </ul>

            </div><!-- ending of side area -->

            <div class="col-sm-10">
                <h2>Edit Post</h2>

                <div><?php echo Message();
                            echo SuccessMessage();
                     ?>
                </div>
                  <?php
                      $idpara = $_GET['edit'];
                      $connectingDB;
                      $query="SELECT * FROM admin_panel WHERE id='$idpara'";
                      $ExecuteQuery=mysql_query($query);
                      while ($Datarows=mysql_fetch_array($ExecuteQuery)) {
                        $blog_title = $Datarows['title'];
                        $blog_category = $Datarows['category'];
                        $blog_image = $Datarows['image'];
                        $blog_post = $Datarows['post'];
                      }

                   ?>

                <form action="edit-post.php?edit=<?php echo $idpara; ?>" method="post" enctype="multipart/form-data">
                    <fieldset>
                        <div class="form-group">
                            <label for="updatetitle">Title:</label>
                            <input type="text" value="<?php echo $blog_title; ?>" class="form-control" name="title" placeholder="title" id="title">
                        </div>
                        <br>
                        <div class="form-group">
                          <span class="fieldinfo">Existing category </span><strong style="color:red;"><?php echo $blog_category; ?></strong>
                          <br>
                            <label for="categoryselect">Category:</label>
                            <select type="text" class="form-control" name="category" id="categoryselect">
                                <?php
                                      //SQL TO EXTRACT db_TABLE PARAMETERS
                                      $Query = "SELECT * FROM category ORDER BY datetime desc";
                                      $Execute=mysql_query($Query);

                                      while($Datarows = mysql_fetch_array($Execute)){
                                          $id=$Datarows['id'];
                                          $category=$Datarows['name'];
                                  ?>

                                  <option><?php echo $category; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <br>
                        <div class="form-group">
                          <span class="fieldinfo">Existing image </span>
                              <img src="upload/<?php echo $blog_image; ?>" width="100" height="70">
                              <br>
                            <label for="imageselect"><span class="fiedinfo">Image:</span></label>
                            <input type="file" class="form-control" name="image" id="imageselect">
                        </div>
                        <br>
                        <div class="form-group">
                            <label for="postselect"><span class="fiedinfo">Post</span></label>
                            <textarea class="form-control" name="post" id="post"><?php echo $blog_post; ?></textarea>
                        </div>
                        <br>
                        <input type="submit" class="btn btn-success btn-block" name="submit" value="Add New Post">
                        <br>
                    </fieldset>

                </form>

                <div>


            </div><!-- ending of main area -->

        </div><!-- ending of row -->
    </div><!-- ending of container -->

    <div id="footer">
        <hr>
        <p>Theme By | Em3 | &copy;2017</p>
        <a style="color: white; text-decoration: none; cusor: pointer; font-weight: bold;"


    </div>











    <!-- jquery -->
   <!-- <script src="js/jquery.js"></script>-->

    <!-- bootstrap js -->
    <script src="js/bootstrap.min.js"></script>

</body>


</html>
